This document briefly explains how one can use the Monte Carlo Approximation of Pi MATLAB code.

User will find 4 functions in the folder which consist of:

- fixed_point(n) -> Input n (int): Number of iterations -random points plotted- to approximate Pi to. Output finalvalue (double): Approximation of Pi given n iterations -random points plotted.

- precision_amount(sig) -> Input sig (int): Number of Significant digits of precision Pi required for approximation. Output finalvalue (double): Approximation of Pi given required precision.

-circle_coord(x_circle) -> Input x_circle (double): Randomly generated X coordinate between 0 and 1. Output y_circle (double): Y coordinate on quarter circle given a specific X coordinate

-frontend() -> Frontend function for user to submit desired level of precision in pi approximation. Output finalvalue (double): Approximation of Pi given required precision.



