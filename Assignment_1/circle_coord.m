function y_circle=circle_coord (x_circle)
    %Input: X-Coordinate of random Point
    %Output: Y coordinate that is on the quarter-circle
    %Solved from our circle equation restricted to first quadrant
    
    if x_circle <0
        error('Random X-Coordinate outside of range');
        %By design random points in (0,1) so error if x<0
    end
    y_circle=sqrt(1-x_circle^2);
    %Solve for y and return
    return
end 