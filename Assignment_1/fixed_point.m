function finalvalue= fixed_point(n)
    %function returns pi approximation given integer n iterations (random
    %points plotted)
    counter=0;
    error=pi;
    %Initalizes counters and error limit as pi
    t1=tic;
    errors=[];
    values=[];
    times=[];
    %Initalizes arrays storing errors, approximations, times
   %points
    
    for i = 1:n
        tic %start timer
        random_pt=[rand,rand]; %generate random pt
       
        if random_pt(2) < circle_coord(random_pt(1))
            %calculate y on equation of quarter circle for given x
            
            counter=counter+1;
            %if y of random pt less inside circle, iterate counter
        end
        
        approx=4*double(counter/i) ;
        %if y of random pt less inside circle, iterate counter
       
        values(end+1)=approx;
        errors(end+1)=(abs(pi-approx));
        times(end+1)=toc+sum(times);
        %update arrays with new  approx, error, and time
    end
    
    
    
    close all;
    f1=figure(1);
    
    %Approximation vs Number of Iterations Plot
    subplot(2,2,1);
    hold on
    plot(values);
    yline(pi)
    title ("Approximation of Pi")
    xlabel("Number of Iterations")
    ylabel("Value of Pi")
    hold off
    
    %Error of Approximation vs Log Error 
    subplot(2,2,2);
    semilogy(errors);
    title ("Error of Approximation")
    xlabel("Number of Iterations")
    ylabel("Log Error")
    
    %Log of Computation Time vs Log of Error (Precision)
    subplot(2,2,3:4)
    loglog(times,errors)
    title ("Precision vs. Cost of Approximation")
    xlabel("Elapsed Time")
    ylabel("Log Error")
    
    sum(times)
    disp(errors(end))
    %Returns final approximation of pi for fixed number of iterations
    finalvalue=approx;
end

