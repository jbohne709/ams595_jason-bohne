function frontend
    %Front end function that provides interaction with user
    
    %Requests user desired approximation for pi and stores it
    prompt_1 = 'What is the precision you wish to approximate Pi to? \n ';
    h = input(prompt_1);
    prompt_2 = 'Please hit enter to approximate. \n(Note a real time plot of random points will pop up)';
    g=input(prompt_2);
    
    %prints pi approximation to required precision
    fprintf(1, '\n');
    fprintf( " The approximate value of Pi is %.4f\n ",round( precision_amount(h),4))
end