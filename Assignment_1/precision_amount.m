function [finalvalue]=precision_amount (sig)
    %function returns pi approximation to desired sig- digit precision
    
   counter=0;
   i=0;
   approx=0;
   %Initalizes counters and approx as 0
    
   errors=[];
   values=[];
   times=[];
   points=[];
   %Initalizes arrays storing errors, approximations, times, and random
   %points
   
    while round(pi  ,sig,'significant')~=round(approx ,sig,'significant')
        tic %start timer
        i=i+1;
        random_pt=[rand,rand]; %generate random pt
        y_coord=circle_coord(random_pt(1)); 
        %calculate y on equation of quarter circle for given x
        
        if random_pt(2) < y_coord
            counter=counter+1;
            %if y of random pt less inside circle, iterate counter
        end
       
        approx=(4*counter/i);
        %approximate pi from prob inside quarter circle
       
        error=abs(pi-approx);
        %update error value
        
        points{end+1}=random_pt ;
        values(end+1)=approx;
        errors(end+1)=error;
        times(end+1)=toc+sum(times);
        %update arrays with new random pt, approx, error, and time
    end
    
    close all;
    f1=figure(1);
    
    %Approximation vs Number of Iterations Plot
    hold on
    subplot(2,2,1);
    plot(values);
    yline(pi)
    title ("Approximation of Pi")
    xlabel("Number of Iterations")
    ylabel("Value of Pi")
    
    %Error of Approximation vs Log Error 
    subplot(2,2,2);
    semilogy(errors);
    title ("Error of Approximation")
    xlabel("Number of Iterations")
    ylabel("Log Error")
    
    %Log of Computation Time vs Log of Error (Precision)
    subplot(2,2,3:4)
    loglog(times,errors)
    title ("Precision vs. Cost of Approximation")
    xlabel("Elapsed Time")
    ylabel("Log Error")
    hold off
  
    %Dynamic point plotting figure
    f2=figure(2);
    
    % quarter cirlce equation with range of x values
    x = linspace(0,1,100);
    y = sqrt(1-x.^2);
    hold on
   
    plot(x,y)
    title("Distribution of Random Points")
    %Iterates through dynamically plotting random points
 
    for j = 1 : length(points)
        f2=figure(2);
        pt=cell2mat(points(1:j));
        pt_x=pt(1:2:end);
        pt_y=pt(2:2:end);
        pt_color=[];
        %determines color each point based on location
         
        %red-> inside cirlce
        %black -> outside
        for k=1 :j
            if pt_x(k)^2+pt_y(k)^2>1
                pt_color(k,1:3)=[0 0 0];
                %RGB Codes
            else
                 pt_color(k,1:3)=[1 0 0];
                 %RGB Codes
            end
        end
        
        scatter(pt_x,pt_y,[],pt_color,'*');
        xlim([0 1])
        ylim([0 1])
        pause(0.03)
        %plots scatter of points pausing between each iteration
    end
    
    display=round(approx,sig,'significant');
   
    %displays approximation of pi on dynamic plot
    xlabel({"Approximation of Pi: "+display},'FontSize',12,'FontWeight','bold')
    hold off
    
    %returns value of approximation up to a defined sig digit level
    finalvalue=[round(approx,sig,'significant')];
   
    
end

