#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

    std::vector <int> factorize(int n) 
    {
        //declares vector of integers, add 1
        std::vector<int> answer;
        answer.push_back(1);
        //starting with counter, iterating up to and including n 
        int counter=2;
        while (counter<=n)
        {
            //if n mod counter=0 then counter is a factor
            if (n%counter==0)
            {
                //add to vector
                answer.push_back(counter);
            }
            counter++;
        }

        return answer;
    }

    //utilizes print vector function here
    void print_vector(std::vector<int> v) {
    int n=0;
 
    try{
        //iterate through the vector up until size of vector
        while (n<v.size()) {
        //print each element
      cout << v[n] << " "  ;
      
        n=n+1;}
      
    }
    catch (exception& e){
       cout<< e.what();
    }
}
    int main () 
    {
        //test cases 
        print_vector(factorize(2));
        cout<<" "<<endl;
        print_vector(factorize(72));
         cout<<" "<<endl;
        print_vector(factorize(196));
    }
