#include <iostream>
#include <ostream>
#include <vector>
#include <string>
using namespace std;

int main(){
    //starting values
    int n0=1;
    int n1=2;
    int temp=n1;
    cout<<n0 <<" ";
    cout<<n1 <<" ";
    //loops through all fib until largest is less than 4,000,000
    while (n0+n1<4000001){
        //prints new fib number
        cout<<n0+n1<<" ";
        //utilizes temp variable to iterate
         temp=n1;
        n1=n0+n1;
        n0=temp;
    }
}