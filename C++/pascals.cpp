#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

 void print_vector(std::vector<int> v) {
    int n=0;
 
    try{
        //iterate through the vector up until size of vector
        while (n<v.size()) {
        //print each element
      cout << v[n] << " "  ;
      
        n=n+1;}
      
    }
    catch (exception& e){
       cout<< e.what();
    }
}
    void pascals(int n)
    {
        //declare our vector of integers for nth line of pascals
        std::vector<int> pascalline;
        int k=1;
        //iterate through each line of pascals
        while(k<n+1)
        {
            if (k==1)
            {
                //if first line intialize with 1
                pascalline.push_back(1);
                print_vector(pascalline);
                cout<<" "<<endl;

            }
            else
            {   
                //create a temp line, type is vec of integers
                std::vector<int> line;
                //iterate current pascals line adding consecutive elements together
                for (int i=0;i<pascalline.size()-1;i++)
                {
                    //append these to temp vector
                    line.push_back(pascalline[i]+pascalline[i+1]);
                }
                //add element of 1 to end of temp
                line.push_back(1);
                //create iterator type at first element
                auto itPos = line.begin() ;
                //insert 1 at start of temp vector
                line.insert(itPos,1);
                //clear vector of current line of pascals
                pascalline.clear();
                for (int i=0;i<line.size();i++)
                {
                //update pascal line with elements from temp
                pascalline.push_back(line[i]);
                }
                print_vector(pascalline);
                cout<<" "<<endl;
               //clear temp vector
               line.clear();
            }
            k++;
            
        }
       
    }
       
    int main () 
    {
         pascals(6);
       
    }