#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;
    
    std::vector <double> discretize(int N)
    {  
        std::vector <double> points;
        for (float i=0; i<N+1;i++)
        {
            float point=1/double(N)*double(i);
            //discretize our points into N intervals 
            points.push_back(point);
        }
        return points;
    }
    
     std::vector <double>  functionval(std::vector<double> v)
    {   
        std::vector<double> value;
        for (int x=0; x<v.size();x++)
        {
            // for each point compute function value
            value.push_back(sqrt(1-pow(v[x],2)));
        }
        return value;
    }
    
    double trap_approx(std::vector<double> v){
        double val=0;
        for (int j =1; j<v.size();j++)
        {
            //iterates through vector of function values, computing trapezoid sum
            val+=(1/(float(v.size()-1)))*(v[j]+v[j-1])/2;   
        }
        return val;
    }
    
    int main ()
    {
        cout<<"Welcome to the Pi Approximation Calculator"<<endl;
        cout<<"To Approximate by Number of Intervals enter 1, For Error Tolerance Enter 2"<<endl;
        
        string mystring;
        cin>> mystring;
        
        std::vector <double> eval_values;
        try
        {
            int number=stoi(mystring);
            switch (number)
            {
            
            case 1:
            //approximate by number of intervals
                cout << "Enter a Number" << " ";
                int N1;
                cin >> N1;
        
                //computes function values of discretization
                eval_values=functionval(discretize(N1));
        
                double approx1;
                approx1=4*trap_approx(eval_values);
               
                cout<<"Approximation is "<<" ";
                cout<<approx1<< endl;
                
                //computes error of approx
                double error1;
                error1=abs(M_PI-approx1);
                cout<<"Error is "<<" ";
                cout <<error1<<endl;
                
                break;
                
            case 2:
            // approximates pi by error tolerance 
               cout << "Enter an Error Tolerance" << " ";
  
                double error2;
                cin >> error2;
        
                double approx2;
                approx2=0;
                int N2;
                N2=3;
                
                //loops through until error tolerance is met
                while (abs(M_PI-approx2)>error2)
                {
           
                    eval_values=functionval(discretize(N2));
                    approx2=4*trap_approx(eval_values);
                    N2=N2+1;
                }
                
                cout<<"Approximation is "<<" ";
                cout<<approx2<< endl;
        
                cout<<"Number of Iterations"<<" ";
                cout<<N2<<endl;
      
                break;

            default:
                cout << "Not a Valid Number " << endl;
        }
    }
    //catch statement if user did not input a number 
        catch (exception e) 
        {
         cout << "Not a Valid Number" << endl;
        }
        
    }