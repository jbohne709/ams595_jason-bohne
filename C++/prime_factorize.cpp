#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;
   
  std::vector <int> factorize(int n) 
    {
        //declares vector of integers, add 1
        std::vector<int> answer;
        answer.push_back(1);
        //starting with counter, iterating up to n 
        int counter=2;
        while (counter<=n)
        {
            //if n mod counter=0 then counter is a factor
            if (n%counter==0)
            {
                //add to vector
                answer.push_back(counter);
            }
            counter++;
        }

        return answer;
    }
    
    bool isprime (int n) 
    {
        bool result = true;
        //if n less than 2, cannot be prime
        if (n < 2)
        {
            result = false;
        }
        else
        {
            //starting at 2, loop through up until sqrt of n
            int counter = 2;
            while (counter < (sqrt (double (n)) + 1))
            {
                //any point a factor which isn't n divides, then is not prime
                if (n % counter == 0 && counter != n)
                {
	                result=false;
                };
            counter++;
            }
        }
        return result;  
    }

    void print_vector(std::vector<int> v) {
    int n=0;
 
    try{
        //iterate through the vector up until size of vector
        while (n<v.size()) {
        //print each element
      cout << v[n] << " "  ;
        n=n+1;}
    }
    catch (exception& e){
       cout<< e.what();
    }
}
     std::vector<int> prime_factorize(int n) 
    {
        //declares vector of integers for prime factorization
        std::vector<int> primeanswer;
        
        //declares and defines vector of integers as all factors of n
        std::vector<int> factors=factorize(n);
        
       
        int k=n;
        //iterating through each factor
        for (int i=0; i<factors.size()+1;i++)
        {
            //if factor is not prime, skip
            if(isprime(factors[i])==0)
            {
              
               continue;
            }
            else
            {   
                //if factor is prime compute number of times each prime appears
                while (k%factors[i]==0 )
                {
                    //add this to our vector of prime factorization
                    primeanswer.push_back(factors[i]);
                    k=k/factors[i];
                }
                k=n;
            }
        }

        return primeanswer;
    }
    int main () 
    {
        //test cases
        print_vector(prime_factorize(2));
        cout<<" "<<endl;
        print_vector(prime_factorize(72));
         cout<<" "<<endl;
        print_vector(prime_factorize(196));
        cout<<" "<<endl;
       
    }