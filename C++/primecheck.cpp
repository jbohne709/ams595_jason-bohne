#include <iostream>
#include <ostream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

    bool isprime (int n) 
    {
        bool result = true;
        //if n less than 2, cannot be prime
        if (n < 2)
        {
            result = false;
        }
        else
        {
            //starting at 2, loop through up until sqrt of n
            int counter = 2;
            while (counter < (sqrt (n) + 1))
            {
                //any point a factor which isn't n divides, then is not prime
                if (n % counter == 0 && counter != n)
                {
	                result=false;
                };
            counter++;
            }
        }
        return result;  
    }

    int main () 
    {
        //test cases
        bool test1 = isprime (2);
        cout << test1 << endl;
        bool test2 = isprime (10);
        cout << test2 << endl;
        bool test3 = isprime (17);
        cout << test3 << endl;
    }