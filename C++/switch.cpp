#include <iostream>
#include <string>
using namespace std;

int main(){
    cout << "Enter a Number" << endl;
    
    string mystring;
    cin >> mystring;
    try{
        //casts string to int, returning exception if not a number
        int number=stoi(mystring);
        // switch statements of possible cases
        switch (number){
            //breaks after each case 
            case -1:
                cout << "Number is negative one" << endl;
                break;
            case 0:
                cout << "Number is zero" << endl;
                break;
            case 1:
                cout << "Number is positive one" << endl;
                break;
            
            default:
                cout << "Number is something else" << endl;
    }
    }
    //catch statement if user did not input a number 
      catch (exception e) {
         cout << "Not a number" << endl;
    }
}